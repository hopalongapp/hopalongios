//
//  AddUserPopupViewController.swift
//  HopAlong
//
//  Created by Ryan on 2/25/16.
//  Copyright © 2016 HopAlong. All rights reserved.
//

import UIKit

protocol AddUserPopupViewControllerDelegate {
    func inviteSelected(selectedUserIDs: [Int], controller: AddUserPopupViewController)
}

class AddUserPopupViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var delegate:AddUserPopupViewControllerDelegate? = nil
    
    var selectedUsers = [Int]()
    
    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func inviteSelected(sender: AnyObject) {
        if delegate != nil {
            delegate!.inviteSelected(self.selectedUsers, controller: self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //turns on keyboard automatically for search bar
        self.searchBar.becomeFirstResponder()
        
        self.tableView.reloadData()

        // Do any additional setup after loading the view.
    }
    
    
    // MARK: - Table View

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Friends"
        } else if section == 1 {
            return "Recents"
        } else {
            return "My Network"
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 3
        } else if section == 1 {
            return 3
        } else {
            return 7
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("UserSearchCell") as! UserSearchCell
        cell.userImage.image
            = UIImage(named: "11143625_10207307429752346_4498797191705964170_n.png")
        cell.name.text = "Ryan Demo"
        cell.info.text = "Electrical and Computer Engineering"
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if (tableView.cellForRowAtIndexPath(indexPath) as! UserSearchCell).accessoryType != .Checkmark {
            (tableView.cellForRowAtIndexPath(indexPath) as! UserSearchCell).accessoryType = .Checkmark
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
            
            let selectedUserBarImage = UIImageView(frame: CGRectMake(0, 0, 24, 24))
            selectedUserBarImage.layer.cornerRadius = selectedUserBarImage.bounds.height/2
            selectedUserBarImage.clipsToBounds = true
            selectedUserBarImage.contentMode = .ScaleAspectFill
            selectedUserBarImage.image = (tableView.cellForRowAtIndexPath(indexPath) as! UserSearchCell).userImage.image
            
            let selectedUserBarButton = UIBarButtonItem(customView: selectedUserBarImage)
            
            self.toolbar.items?.insert(selectedUserBarButton, atIndex: 0)
        } else {
            
            (tableView.cellForRowAtIndexPath(indexPath) as! UserSearchCell).accessoryType = .None
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
            
            self.toolbar.items?.removeAtIndex(0) //TODO: change to index of selected
            
        }
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

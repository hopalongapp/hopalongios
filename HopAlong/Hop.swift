//
//  Hop.swift
//  HopAlong
//
//  Created by Ryan on 3/7/16.
//  Copyright © 2016 HopAlong. All rights reserved.
//

import Foundation
import CoreLocation

class Hop {
    
    var hopID: Int
    var name: String
    var departureDate: NSDate
    var currentRiders: Int
    var maxRiders: Int
    var pickupLocation: CLLocationCoordinate2D
    var pickupLocationName: String
    var dropOffLocation: CLLocationCoordinate2D
    var dropOffLocationName: String
    var associatedSchoolID: Int
    var groupLeaderID: Int
    var riders = [[String: AnyObject]]() //id, name, fieldOfStudy, phoneNumber, bio, pictureURL, verified
    
    init() {
        self.hopID = 0
        self.name = ""
        self.departureDate = NSDate()
        
        self.pickupLocation = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
        self.pickupLocationName = ""
        self.dropOffLocation = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
        self.dropOffLocationName = ""
        self.riders = [["":""]]
        
        self.associatedSchoolID = 0
        self.groupLeaderID = 0
        self.currentRiders = self.riders.count
        self.maxRiders = 4
    }
    
    init(hopID: String, name: String, departureDate: NSDate, pickupLocation: CLLocationCoordinate2D, pickupLocationName: String, dropOffLocation: CLLocationCoordinate2D, dropOffLocationName: String, riders: [[String: AnyObject]], associatedSchoolID: Int) {
        self.hopID = 0
        self.name = name
        self.departureDate = departureDate

        self.pickupLocation = pickupLocation
        self.pickupLocationName = pickupLocationName
        self.dropOffLocation = dropOffLocation
        self.dropOffLocationName = dropOffLocationName
        self.riders = riders
        
        self.associatedSchoolID = associatedSchoolID
        self.groupLeaderID = riders[0]["id"]! as! Int
        self.currentRiders = self.riders.count
        self.maxRiders = 4
    }
    
    func addRider(riderID: String) -> Bool {
        return false
    }
    
    func removeRider(riderID: String) -> Bool {
        return false
    }
    
}
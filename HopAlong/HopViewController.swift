//
//  HopViewController.swift
//  HopAlong
//
//  Created by Ryan on 2/24/16.
//  Copyright © 2016 HopAlong. All rights reserved.
//

import UIKit
import MapKit

class HopViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var thisHop = Hop()
    var minutesToDestination = 0
    var expectedArrivalDate = NSDate()
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var date: UILabel!
    
    @IBAction func infoButton(sender: AnyObject) {
        //info button action
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let request = MKDirectionsRequest()
        request.source = MKMapItem(placemark: MKPlacemark(coordinate: thisHop.pickupLocation, addressDictionary: nil))
        request.destination = MKMapItem(placemark: MKPlacemark(coordinate: thisHop.dropOffLocation, addressDictionary: nil))
        request.requestsAlternateRoutes = false
        request.departureDate = thisHop.departureDate
        
        MKDirections(request: request).calculateETAWithCompletionHandler { (response, error) -> Void in
            if error != nil {
                print(error)
            } else {
                self.minutesToDestination = Int((Double((response?.expectedTravelTime)!) % 3600) / 60)
                self.expectedArrivalDate = (response?.expectedArrivalDate)!
                self.tableView.reloadData()
            }
        }
        
        let formatter = NSDateFormatter()
        formatter.dateFormat = "E, MMM. d"

        self.name.text = self.thisHop.name
        self.date.text = formatter.stringFromDate(self.thisHop.departureDate)
        
        self.view.layer.masksToBounds = true
        self.view.layer.cornerRadius = 10
        self.view.layer.shadowOpacity = 0.4
        self.view.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.view.layer.shadowRadius = 3
        
        //cell height doesn't work!
        self.tableView.layer.masksToBounds = true
        self.tableView.estimatedRowHeight = 110
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.reloadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //table view functions
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let formatter = NSDateFormatter()
        formatter.dateFormat = "h:mm a"
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("TimeLocationCell") as! TimeLocationCell
            cell.pickupName.text = self.thisHop.pickupLocationName
            cell.pickupTime.text = formatter.stringFromDate(self.thisHop.departureDate).lowercaseString
            cell.dropOffName.text = self.thisHop.dropOffLocationName
            cell.dropOffTime.text = formatter.stringFromDate(self.expectedArrivalDate).lowercaseString
            return cell
            
        }
        
        let pricePerPerson = 12.56
        
        
        let cell = tableView.dequeueReusableCellWithIdentifier("RideEstimationCell") as! RideEstimationCell
        cell.rideCapacityLabel.text = "\(thisHop.riders.count)/\(thisHop.maxRiders)"
        cell.pricePerPersonLabel.text = "$\(pricePerPerson)"
        cell.timeToDestinationLabel.text = "\(minutesToDestination) min"
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 110
        }
        return 56
    }
    
    func inviteSelected(selectedUserIDs: [Int]) {
        
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}

//
//  MainViewController.swift
//  HopAlong
//
//  Created by Ryan on 2/23/16.
//  Copyright © 2016 HopAlong. All rights reserved.
//

import UIKit
import MapKit

class MainViewController: UIViewController, MKMapViewDelegate, UIPopoverPresentationControllerDelegate, AddUserPopupViewControllerDelegate {

    var hopContainer: HopViewController = HopViewController()
    
    var addUserActive = false
    var offset = CGFloat()
    
    var editingLocation = false
    let annotation = MKPointAnnotation()
    var mapEditButton = UIButton()
    
    var hops = [Hop]()
    
    var userPicturesMapOverlay = [UserPictureButton]()
    
    @IBOutlet weak var map: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "HopAlong"
        
        self.hops.append(Hop(hopID: "5",
            name: "Hop to JHU",
            departureDate: NSDate(timeIntervalSinceNow: 3600),
            pickupLocation: CLLocationCoordinate2DMake(39.3284, -76.6161),
            pickupLocationName: "Johns Hopkins",
            dropOffLocation: CLLocationCoordinate2DMake(39.177005, -76.668435),
            dropOffLocationName: "BWI Airport",
            riders: [
                ["id":1, "name":"Tushar Lukshminarayanen", "fieldOfStudy":"Electrical and Computer Engineering", "phoneNumber":"6504921534", "bio":".@WhiteHouse: “If we make it easier for more foreign visitors to visit... (it) grows the #economy.” Pass the #JOLTAct http://tiny.cc/PassJOLT", "pictureURL":"https://scontent.xx.fbcdn.net/hprofile-xft1/v/t1.0-1/c120.0.480.480/p480x480/11143625_10207307429752346_4498797191705964170_n.jpg?oh=04d0232c1b04cf91d1b632abc1d7f14f&oe=576DFF74","status":0],
                ["id":2, "name":"Chanan Walia", "fieldOfStudy":"Electrical and Computer Engineering", "phoneNumber":"6504921534", "bio":".@WhiteHouse: “If we make it easier for more foreign visitors to visit... (it) grows the #economy.” Pass the #JOLTAct http://tiny.cc/PassJOLT", "pictureURL":"https://scontent.xx.fbcdn.net/hphotos-xla1/v/t1.0-9/12299354_467335476786435_2469093233497601603_n.jpg?oh=07bf12383a04d88f1df2b20406b8bc8b&oe=574F818B", "status":1],
                ["id":3, "name":"Raymond Peck", "fieldOfStudy":"Gender Studies", "phoneNumber":"6504921534", "bio":".@WhiteHouse: “If we make it easier for more foreign visitors to visit... (it) grows the #economy.” Pass the #JOLTAct http://tiny.cc/PassJOLT", "pictureURL":"https://scontent.xx.fbcdn.net/hphotos-xft1/v/t1.0-9/12301517_10206960574788189_7591700547598955016_n.jpg?oh=57409053e07d618549d5c6f7851b26a1&oe=575B9E9C", "status":2]], associatedSchoolID: 0))
        
        //configure HopViewController
        self.hopContainer = self.storyboard?.instantiateViewControllerWithIdentifier("HopViewController") as! HopViewController
        self.hopContainer.thisHop = hops[0]

        self.addChildViewController(self.hopContainer)
        self.hopContainer.view.frame = CGRectMake(16, 16, self.view.bounds.width - 32, 300)
        self.view.addSubview(self.hopContainer.view)

        self.hopContainer.didMoveToParentViewController(self)

        self.createUserCircles()
        
        // configure map
        self.map.delegate = self
        self.map.rotateEnabled = false
        
        let location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(hops[0].pickupLocation.latitude + 0.0023, hops[0].pickupLocation.longitude - 0.0005) //lat, long; need to account for not north america by adding the shift multiplied by the sign of the coordinate component
        
        let region:MKCoordinateRegion = MKCoordinateRegionMake(location, MKCoordinateSpanMake(CLLocationDegrees(0.008), CLLocationDegrees(0.008)))
        self.map.setRegion(region, animated: true)

        self.annotation.coordinate = CLLocationCoordinate2DMake(hops[0].pickupLocation.latitude, hops[0].pickupLocation.longitude)
        self.annotation.title = "Pickup Location"
        self.annotation.subtitle = self.hops[0].pickupLocationName
        
        self.mapEditButton = UIButton(frame: CGRectMake(0, 0, 51, 51))
        self.mapEditButton.setImage(UIImage(named: "pencil_tip_filled")?.imageWithRenderingMode(.AlwaysTemplate), forState: .Normal)
        self.mapEditButton.backgroundColor = self.view.tintColor //UIColor(red: 241.0/255, green: 196.0/255, blue: 15.0/255, alpha: 1.0)
        self.mapEditButton.tintColor = UIColor.whiteColor()
        self.mapEditButton.addTarget(self, action: "editLocation:", forControlEvents: .TouchUpInside)

        self.map.addAnnotation(self.annotation)
        self.map.selectAnnotation(self.annotation, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /********************************************************************************************/
    // MARK: - User Button Initialization
    
    func createUserCircles() {

        for var i = 0; i < hops[0].riders.count; i++ {
            let button = UserPictureButton(frame: CGRectMake(16, self.hopContainer.view.frame.maxY + CGFloat((48 + 15) * i) - 48, 48, 48), userNumberOnScreen: i, drawStroke: true, riderType: UserPictureButton.RiderStatus(rawValue: (self.hops[0].riders[i]["status"]! as! Int))!)
            
            NSURLSession.sharedSession().dataTaskWithURL(NSURL(string: self.hops[0].riders[i]["pictureURL"]! as! String)!, completionHandler: { (data, response, error) -> Void in
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    if error != nil {
                        print("error: \(error)")
                    } else {
                        button.setImage(UIImage(data: data!), forState: .Normal)
                    }
                })
            }).resume()
            //enable caching of user images
            
            button.addTarget(self, action: "imageTapped:", forControlEvents: UIControlEvents.TouchUpInside)
            
            self.userPicturesMapOverlay.append(button)
            self.view.addSubview(self.userPicturesMapOverlay[i])
        }
        
        let addButton = UserPictureButton(frame: CGRectMake(16, self.hopContainer.view.frame.maxY + CGFloat((48 + 15) * self.userPicturesMapOverlay.count) - 48, 48, 48), userNumberOnScreen: self.userPicturesMapOverlay.count + 1, drawStroke: false, riderType: .None)
        addButton.setImage(UIImage(named: "add-button.png"), forState: .Normal)
        addButton.addTarget(self, action: "addUserToHop:", forControlEvents: UIControlEvents.TouchUpInside)
        self.userPicturesMapOverlay.append(addButton)
        self.view.addSubview(self.userPicturesMapOverlay[self.userPicturesMapOverlay.count - 1])
    }
    
    
    func imageTapped(sender: AnyObject) {
        
        let senderNumber = (sender as! UserPictureButton).userNumberOnScreen
        
        let userInfoPopup = self.storyboard?.instantiateViewControllerWithIdentifier("UserInfoPopupViewController") as! UserInfoPopupViewController
        
        userInfoPopup.name = hops[0].riders[senderNumber]["name"]! as! String
        userInfoPopup.fieldOfStudy = hops[0].riders[senderNumber]["fieldOfStudy"]! as! String
        userInfoPopup.phoneNumber = hops[0].riders[senderNumber]["phoneNumber"]! as! String
        userInfoPopup.bio = hops[0].riders[senderNumber]["bio"]! as! String
        
        userInfoPopup.modalPresentationStyle = .Popover
        userInfoPopup.preferredContentSize = CGSizeMake(self.view.bounds.width - 80 - 24, 290)
        
        let userInfoPopupController = userInfoPopup.popoverPresentationController
        userInfoPopupController?.permittedArrowDirections = .Left
        userInfoPopupController?.delegate = self
        userInfoPopupController?.sourceView = self.userPicturesMapOverlay[senderNumber]
        userInfoPopupController?.sourceRect = CGRectMake(self.userPicturesMapOverlay[senderNumber].frame.minX, 0, 43, 50)
        
        self.presentViewController(userInfoPopup, animated: true, completion: nil)

    }
    
    func addUserToHop(sender: AnyObject) {
        
        self.offset = (sender as! UserPictureButton).frame.minY - 15
        
        UIView.animateWithDuration(0.35, animations: { () -> Void in
            
            self.hopContainer.view.transform = CGAffineTransformMakeTranslation(0, CGFloat(-1 * (self.offset + 3)))
            
            for var i = 0; i < self.userPicturesMapOverlay.count - 1; i++ {
                self.userPicturesMapOverlay[i].transform = CGAffineTransformMakeTranslation(0, CGFloat(-1 * (self.offset + 3)))
            }
            
            (sender as! UserPictureButton).transform = CGAffineTransformMakeTranslation(0, CGFloat(-1 * self.offset))
            
            }) { (complete) -> Void in
                
                let addUserPopup = self.storyboard?.instantiateViewControllerWithIdentifier("AddUserPopupViewController") as! AddUserPopupViewController

                addUserPopup.modalPresentationStyle = .Popover
                addUserPopup.preferredContentSize = CGSizeMake(self.view.bounds.width - 80 - 24, self.view.bounds.height * (2/3))
                addUserPopup.delegate = self
                
                let addUserPopupController = addUserPopup.popoverPresentationController
                addUserPopupController?.permittedArrowDirections = .Left
                addUserPopupController?.delegate = self
                addUserPopupController?.sourceView = sender as! UserPictureButton
                addUserPopupController?.sourceRect = CGRectMake((sender as! UserPictureButton).frame.minX, 0, 43, 50)
                
                self.map.deselectAnnotation(self.annotation, animated: true)
                self.presentViewController(addUserPopup, animated: true, completion: nil)
                
                self.addUserActive = true
                
                //(sender as! UserPictureButton).transform = CGAffineTransformMakeRotation(CGFloat(M_PI_4))
        }
    }
    
    
    /********************************************************************************************/
    // MARK: - Map and Location
    
    func editLocation(sender: AnyObject) {
        
        if editingLocation {
            self.hops[0].pickupLocation = self.map.centerCoordinate
            let location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(hops[0].pickupLocation.latitude + 0.0023, hops[0].pickupLocation.longitude - 0.0005) //lat, long; need to account for not north america by adding the shift multiplied by the sign of the coordinate component
            
            let region:MKCoordinateRegion = MKCoordinateRegionMake(location, MKCoordinateSpanMake(CLLocationDegrees(0.008), CLLocationDegrees(0.008)))
            self.map.setRegion(region, animated: true)
            
            self.mapEditButton.setImage(UIImage(named: "pencil_tip_filled")?.imageWithRenderingMode(.AlwaysTemplate), forState: .Normal)
            self.annotation.coordinate = CLLocationCoordinate2DMake(hops[0].pickupLocation.latitude, hops[0].pickupLocation.longitude)
            self.annotation.title = "Pickup Location"
            self.annotation.subtitle = self.hops[0].pickupLocationName
        } else {
            let region:MKCoordinateRegion = MKCoordinateRegionMake(self.hops[0].pickupLocation, MKCoordinateSpanMake(CLLocationDegrees(0.008), CLLocationDegrees(0.008)))
            self.map.setRegion(region, animated: true)
            
            self.mapEditButton.setImage(UIImage(named: "down_filled")?.imageWithRenderingMode(.AlwaysTemplate), forState: .Normal)
            self.annotation.coordinate = CLLocationCoordinate2DMake(self.hops[0].pickupLocation.latitude, self.hops[0].pickupLocation.longitude)
            self.annotation.title = "Edit Pickup Location"
            self.annotation.subtitle = ""
        }
        
        self.editingLocation = !self.editingLocation
        
        UIView.animateWithDuration(0.35, animations: { () -> Void in
            
            self.hopContainer.view.transform = CGAffineTransformMakeTranslation(0, CGFloat(-1 * (self.hopContainer.view.frame.maxY + 10)))
            
            for var i = 0; i < self.userPicturesMapOverlay.count; i++ {
                self.userPicturesMapOverlay[i].transform = CGAffineTransformMakeTranslation(-1 * (self.userPicturesMapOverlay[i].frame.maxX + 3), 0)
            }
            }) { (complete) -> Void in
                //
        }
        self.map.selectAnnotation(self.annotation, animated: true)
    }
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        var view = mapView.dequeueReusableAnnotationViewWithIdentifier("PickupPin") as? MKPinAnnotationView
        if view == nil {
            view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "PickupPin")
            view!.canShowCallout = true
            view!.pinTintColor = self.view.tintColor//UIColor(red: 241.0/255, green: 196.0/255, blue: 15.0/255, alpha: 1.0)
            view!.animatesDrop = true
        } else {
            view!.annotation = annotation
        }

        view?.leftCalloutAccessoryView = nil
        view?.rightCalloutAccessoryView = self.mapEditButton
        
        return view
    }
    
    func mapView(mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        if editingLocation {
            self.annotation.coordinate = mapView.centerCoordinate
            
        }
    }
    
    /********************************************************************************************/
    // MARK: - Popover Presentation

    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.None
    }
    
    func popoverPresentationControllerDidDismissPopover(popoverPresentationController: UIPopoverPresentationController) {
        if self.addUserActive {
            
            UIView.animateWithDuration(0.35, delay: 0, options: UIViewAnimationOptions.BeginFromCurrentState, animations: { () -> Void in
                //self.userPicturesMapOverlay[self.userPicturesMapOverlay.count - 1].transform = CGAffineTransformMakeRotation(CGFloat(M_PI_4))
                
                self.hopContainer.view.transform = CGAffineTransformMakeTranslation(0, CGFloat(-1 * (self.offset + 3)))
                
                for var i = 0; i < self.userPicturesMapOverlay.count - 1; i++ {
                    self.userPicturesMapOverlay[i].transform = CGAffineTransformMakeTranslation(0, CGFloat(-1 * (self.offset + 3)))
                }
                
                self.userPicturesMapOverlay[self.userPicturesMapOverlay.count - 1].transform = CGAffineTransformMakeTranslation(0, CGFloat(-1 * self.offset))
                
                self.map.selectAnnotation(self.annotation, animated: true)
                }, completion: { (complete) -> Void in
                    self.addUserActive = false
            })


        }
    }
    
    func inviteSelected(selectedUserIDs: [Int], controller: AddUserPopupViewController) {
        controller.dismissViewControllerAnimated(true) { () -> Void in
            
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

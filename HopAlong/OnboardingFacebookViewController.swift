//
//  OnboardingFacebookViewController.swift
//  HopAlong
//
//  Created by Ryan on 2/23/16.
//  Copyright © 2016 HopAlong. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class OnboardingFacebookViewController: UIViewController {

    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var noFacebookButton: UIButton!
    
    @IBAction func loginButtonTapped(sender: AnyObject) {
        let login = FBSDKLoginManager()
        login.logInWithReadPermissions(["public_profile", "email", "user_friends", "user_education_history", "user_events"], fromViewController: self) { (result, error) -> Void in
            if error != nil {
                print(error)
            } else {
                var firstName = ""
                var lastName = ""
                var college = ""
                var major = ""
                var gradYear = ""
                var pictureURL = ""
                
                
                //get facebook data
                if FBSDKAccessToken.currentAccessToken() != nil {
                    let request = FBSDKGraphRequest.init(graphPath: "me", parameters: ["fields":"first_name,last_name,email,education,picture.width(480).height(480).type(square)"], HTTPMethod: "GET")
                    request.startWithCompletionHandler({ (connection, result, error) -> Void in
                        if error != nil {
                            print(error)
                        } else {
                            let resultContent = result as? [String:AnyObject]
                            
                            if let first = resultContent?["first_name"] as? String {
                                firstName = first
                            }
                            
                            if let last = resultContent?["last_name"] as? String {
                                lastName = last
                            }
                            
                            if let edu = resultContent?["education"] as? [[String:AnyObject]] {
                                for item in edu {
                                    if item["type"] as? String == "College" {
                                        college = item["school"]!["name"]! as! String
                                        if let concentration = item["concentration"] {
                                            major = concentration[0]["name"] as! String
                                        }
                                        if let yr = item["year"] {
                                            gradYear = yr["name"] as! String
                                        }
                                    }
                                }
                            }
                            
                            if let pic = resultContent?["picture"] as? [String:AnyObject] {
                                pictureURL = pic["data"]!["url"] as! String
                            }
                            
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                //instantiate sign up view controller and prepopulate fields as available
                                let signUpView = self.storyboard?.instantiateViewControllerWithIdentifier("OnboardingSignUpNavViewController") as? OnboardingSignUpNavViewController
                                signUpView?.incomingFirstName = firstName
                                signUpView?.incomingLastName = lastName
                                signUpView?.incomingMajor = major
                                signUpView?.incomingYear = gradYear
                                signUpView?.incomingSchool = college
                                signUpView?.userFBPhotoURL = pictureURL
                                
                                self.presentViewController(signUpView!, animated: true, completion: nil)
                            })
                            
                        }
                        
                    })
                }
            }
        }
    }
    
    @IBAction func noFacebook(sender: AnyObject) {
        let doubleCheck = UIAlertController(title: "Are you sure?", message: "Continuing with Facebook gives you a better experience using HopAlong", preferredStyle: .Alert)
        doubleCheck.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        doubleCheck.addAction(UIAlertAction(title: "I'm sure", style: .Default, handler: { (action) -> Void in
            self.performSegueWithIdentifier("ContinueNoFB", sender: self)
        }))
        
        self.presentViewController(doubleCheck, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //uncomment to round buttons
        //self.logInButton.layer.cornerRadius = self.logInButton.bounds.height/2
        //self.noFacebookButton.layer.cornerRadius = self.noFacebookButton.bounds.height/2
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

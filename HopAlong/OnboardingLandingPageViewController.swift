//
//  OnboardingLandingPageViewController.swift
//  HopAlong
//
//  Created by Ryan on 2/23/16.
//  Copyright © 2016 HopAlong. All rights reserved.
//

import UIKit

class OnboardingLandingPageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        //implement UIPageViewControllerDataSource and Delegate in self
        self.delegate = self
        self.dataSource = self
        
        self.setViewControllers([self.viewControllerAtIndex(0)!], direction: .Forward, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func viewControllerAtIndex(index: Int) -> UIViewController? {
        if index == 0 {
            return (self.storyboard?.instantiateViewControllerWithIdentifier("Onboarding1ViewController"))!
        } else if index == 1 {
            return (self.storyboard?.instantiateViewControllerWithIdentifier("Onboarding2ViewController"))!
        } else if index == 2 {
            return (self.storyboard?.instantiateViewControllerWithIdentifier("Onboarding3ViewController"))!
        }
        
        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        
        if viewController is Onboarding2ViewController {
            return viewControllerAtIndex(0)
        } else if viewController is Onboarding3ViewController {
            return viewControllerAtIndex(1)
        }
        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        if viewController is Onboarding1ViewController {
            return viewControllerAtIndex(1)
        } else if viewController is Onboarding2ViewController {
            return viewControllerAtIndex(2)
        }
        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, willTransitionToViewControllers pendingViewControllers: [UIViewController]) {
    }
    
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        // The number of items reflected in the page indicator.
        return 3
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        // The selected item reflected in the page indicator.
        return 0
    }
    
    func pageViewControllerSupportedInterfaceOrientations(pageViewController: UIPageViewController) -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.Portrait
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

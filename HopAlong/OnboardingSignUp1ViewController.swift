//
//  OnboardingSignUp1ViewController.swift
//  HopAlong
//
//  Created by Ryan on 2/23/16.
//  Copyright © 2016 HopAlong. All rights reserved.
//

import UIKit

class OnboardingSignUp1ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var currentSearch = ""
    var searchResults = [[String:String]]()
    
    var schoolChosen = false
    var chosenSchool = [String:String]()
    
    var searchController: UISearchController!
    
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBarHidden = true
        
        if (self.navigationController as! OnboardingSignUpNavViewController).incomingSchool != "" {
            self.questionLabel.text = "Choose your school"
            self.currentSearch = (self.navigationController as! OnboardingSignUpNavViewController).incomingSchool
            self.reloadSchoolList(self)
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reloadSchoolList(sender: AnyObject) {
        let url = NSURL(string: "https://inventory.data.gov/api/action/datastore_search_sql?sql=SELECT+%22UNITID%22%2C+%22INSTNM%22%2C+%22ADDR%22%2C+%22CITY%22%2C+%22STABBR%22%2C+%22ZIP%22%2C+%22WEBADDR%22+from+%2238625c3d-5388-4c16-a30f-d105432553a4%22+WHERE+%22INSTNM%22+ILIKE+%27%25\(self.currentSearch.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)%25%27+LIMIT+10")!
        
        let urlConfig = NSURLSessionConfiguration.defaultSessionConfiguration()
        urlConfig.timeoutIntervalForRequest = 10
        urlConfig.timeoutIntervalForResource = 10
        
        do {
            let stuff = try NSJSONSerialization.JSONObjectWithData(NSData(contentsOfURL: url)!, options: NSJSONReadingOptions.MutableContainers)
            if let results = stuff["result"]!!["records"]! as? [[String:String]] {
                self.searchResults = results
                self.tableView.reloadData()
            }
        } catch {
            
        }
        
    }
    
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if schoolChosen {
            /*UIView.animateWithDuration(0.3, animations: {
                self.tableView.frame = CGRect(x: self.tableView.frame.minX, y: self.tableView.frame.minY, width: self.tableView.frame.width, height: 44)
                self.searchBar.alpha = 0
            })
            self.searchBar.hidden = true*/
            return 1
        }
        return self.searchResults.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        
        if schoolChosen {
            cell.textLabel!.text = self.chosenSchool["INSTNM"]
            cell.detailTextLabel!.text = self.chosenSchool["CITY"]! + ", " + self.chosenSchool["STABBR"]!
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        } else {
            cell.textLabel!.text = self.searchResults[indexPath.row]["INSTNM"]
            cell.detailTextLabel!.text = self.searchResults[indexPath.row]["CITY"]! + ", " + self.searchResults[indexPath.row]["STABBR"]!
            cell.accessoryType = UITableViewCellAccessoryType.None
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if schoolChosen {
            self.schoolChosen = false
            self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
            /*self.searchBar.hidden = false
            UIView.animateWithDuration(0.3, animations: {
                self.tableView.frame = CGRect(x: self.tableView.frame.minX, y: self.tableView.frame.minY, width: self.tableView.frame.width, height: 44*5)
                self.searchBar.alpha = 1
            })*/
        } else {
            self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
            self.chosenSchool = self.searchResults[indexPath.row]
            self.schoolChosen = true
            self.tableView.reloadData()
        }
    }

    /*func configureSearchController() {
        searchController = UISearchController(searchResultsController: nil)
        
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = true
        searchController.searchBar.placeholder = "Search School"
        searchController.searchBar.delegate = self

    }*/
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate
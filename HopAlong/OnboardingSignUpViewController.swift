//
//  OnboardingSignUpViewController.swift
//  HopAlong
//
//  Created by Ryan on 2/23/16.
//  Copyright © 2016 HopAlong. All rights reserved.
//

import UIKit

class OnboardingSignUpViewController: UIViewController, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate {

    var currentSearch = ""
    var searchResults = [[String:String]]()
    
    var schoolChosen = false
    var chosenSchool = [String:String]()
    
    var connectedToFB = false
    
    var incomingFirstName = ""
    var incomingLastName = ""
    var incomingMajor = ""
    var incomingYear = ""
    var incomingSchool = ""
    var userFBPhotoURL = ""
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var userPicture: UIImageView!
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    
    @IBOutlet weak var majorField: UITextField!
    @IBOutlet weak var yearField: UITextField!
    @IBOutlet weak var campusInvolvementField: UITextField!
    
    @IBAction func register(sender: AnyObject) {
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: "onboardingComplete")
        NSUserDefaults.standardUserDefaults().synchronize()
        
        self.performSegueWithIdentifier("ToWelcome", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Sign Up"
        
        self.userPicture.layer.cornerRadius = self.userPicture.frame.size.width/2
        self.userPicture.layer.masksToBounds = true

        self.incomingFirstName = (self.navigationController as! OnboardingSignUpNavViewController).incomingFirstName
        self.incomingLastName = (self.navigationController as! OnboardingSignUpNavViewController).incomingLastName
        self.incomingMajor = (self.navigationController as! OnboardingSignUpNavViewController).incomingMajor
        self.incomingYear = (self.navigationController as! OnboardingSignUpNavViewController).incomingYear
        self.incomingSchool = (self.navigationController as! OnboardingSignUpNavViewController).incomingSchool
        self.userFBPhotoURL = (self.navigationController as! OnboardingSignUpNavViewController).userFBPhotoURL
        
        self.firstNameField.text = self.incomingFirstName
        self.lastNameField.text = self.incomingLastName
        self.majorField.text = self.incomingMajor
        self.yearField.text = self.incomingYear
        
        //do something with incoming school and picture
        if let url = NSURL(string: self.userFBPhotoURL) {
            if let data = NSData(contentsOfURL: url) {
                userPicture.image = UIImage(data: data)
            }
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        // to limit network activity, reload half a second after last key press.
        if searchText.characters.count > 0 {
            self.currentSearch = searchText
            NSObject.cancelPreviousPerformRequestsWithTarget(self, selector: "reloadSchoolList:", object: nil)
            self.performSelector("reloadSchoolList:", withObject: nil, afterDelay: 0.5)
        } else {
            self.currentSearch = searchText
            searchResults.removeAll()
            self.tableView.reloadData()
        }
    }
    
    func reloadSchoolList(sender: AnyObject) {
        //query national college database
        let url = NSURL(string: "https://inventory.data.gov/api/action/datastore_search_sql?sql=SELECT+%22UNITID%22%2C+%22INSTNM%22%2C+%22ADDR%22%2C+%22CITY%22%2C+%22STABBR%22%2C+%22ZIP%22%2C+%22WEBADDR%22+from+%2238625c3d-5388-4c16-a30f-d105432553a4%22+WHERE+%22INSTNM%22+ILIKE+%27%25\(self.currentSearch.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)%25%27+LIMIT+10")!
        
        let urlConfig = NSURLSessionConfiguration.defaultSessionConfiguration()
        urlConfig.timeoutIntervalForRequest = 10
        urlConfig.timeoutIntervalForResource = 10
        
        do {
            let stuff = try NSJSONSerialization.JSONObjectWithData(NSData(contentsOfURL: url)!, options: NSJSONReadingOptions.MutableContainers)
            if let results = stuff["result"]!!["records"]! as? [[String:String]] {
                self.searchResults = results
                self.tableView.reloadData()
            }
        } catch {
            
        }
        
    }
    
    func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            self.view.frame.origin.y -= keyboardSize.height
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            self.view.frame.origin.y += keyboardSize.height
        }
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if schoolChosen {
            UIView.animateWithDuration(0.3, animations: {
                self.tableView.frame = CGRect(x: self.tableView.frame.minX, y: self.tableView.frame.minY, width: self.tableView.frame.width, height: 44)
                self.searchBar.alpha = 0
            })
            self.searchBar.hidden = true
            return 1
        }
        return self.searchResults.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        
        if schoolChosen {
            cell.textLabel!.text = self.chosenSchool["INSTNM"]
            cell.detailTextLabel!.text = self.chosenSchool["CITY"]! + ", " + self.chosenSchool["STABBR"]!
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        } else {
            cell.textLabel!.text = self.searchResults[indexPath.row]["INSTNM"]
            cell.detailTextLabel!.text = self.searchResults[indexPath.row]["CITY"]! + ", " + self.searchResults[indexPath.row]["STABBR"]!
            cell.accessoryType = UITableViewCellAccessoryType.None
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if schoolChosen {
            self.schoolChosen = false
            self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
            self.searchBar.hidden = false
            UIView.animateWithDuration(0.3, animations: {
                self.tableView.frame = CGRect(x: self.tableView.frame.minX, y: self.tableView.frame.minY, width: self.tableView.frame.width, height: 44*5)
                self.searchBar.alpha = 1
            })
        } else {
            self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
            self.chosenSchool = self.searchResults[indexPath.row]
            self.schoolChosen = true
            self.tableView.reloadData()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

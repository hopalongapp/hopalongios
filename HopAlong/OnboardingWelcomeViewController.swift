//
//  OnboardingWelcomeViewController.swift
//  HopAlong
//
//  Created by Ryan on 2/23/16.
//  Copyright © 2016 HopAlong. All rights reserved.
//

import UIKit

class OnboardingWelcomeViewController: UIViewController {

    @IBOutlet weak var inviteText: UILabel!
    
    @IBAction func invite(sender: AnyObject) {
        self.performSegueWithIdentifier("OnboardingCompleteToApp", sender: self)
    }
    
    @IBAction func moveToApp(sender: AnyObject) {
        self.performSegueWithIdentifier("OnboardingCompleteToApp", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        var numUsersAtSchool = 0
        
        if numUsersAtSchool < 1 {
            inviteText.text = "You're the first on campus to sign up for HopAlong. Invite some friends to share the convenience!"
        } else {
            inviteText.text = "You're joining a community of \(numUsersAtSchool) classmates on HopAlong. Invite some friends to share the convenience!"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

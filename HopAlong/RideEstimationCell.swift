//
//  RideEstimationCell.swift
//  HopAlong
//
//  Created by Ryan on 3/8/16.
//  Copyright © 2016 HopAlong. All rights reserved.
//

import UIKit

class RideEstimationCell: UITableViewCell {

    var numRiders = 1
    var maxRiders = 4
    var pricePerPerson = 12.56
    var minutesToDestination = 15
    
    @IBOutlet weak var rideCapacityLabel: UILabel!
    
    @IBOutlet weak var pricePerPersonLabel: UILabel!
    
    @IBOutlet weak var timeToDestinationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
//        rideCapacityLabel.text = "\(numRiders)/\(maxRiders)"
//        pricePerPersonLabel.text = "$\(pricePerPerson)"
//        timeToDestinationLabel.text = "\(minutesToDestination) min"
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

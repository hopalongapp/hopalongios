//
//  TimeLocationCell.swift
//  HopAlong
//
//  Created by Ryan on 2/24/16.
//  Copyright © 2016 HopAlong. All rights reserved.
//

import UIKit

class TimeLocationCell: UITableViewCell {
    
    @IBOutlet weak var pickupName: UILabel!
    @IBOutlet weak var dropOffName: UILabel!
    @IBOutlet weak var pickupTime: UILabel!
    @IBOutlet weak var dropOffTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.createStartCircle()
        self.createEndCircle()
        
        let imageSize = CGSize(width: 1, height: 32)
        let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 32.5, y: 27), size: imageSize))
        imageView.backgroundColor = UIColor.lightGrayColor()
        self.addSubview(imageView)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func createStartCircle() {
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: 33,y: 17), radius: CGFloat(8), startAngle: CGFloat(0), endAngle:CGFloat(M_PI * 2), clockwise: true)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.CGPath
        
        //change the fill color
        shapeLayer.fillColor = UIColor.clearColor().CGColor
        //you can change the stroke color
        shapeLayer.strokeColor = self.tintColor.CGColor
        //you can change the line width
        shapeLayer.lineWidth = 1.0
        
        self.layer.addSublayer(shapeLayer)
        
        
        let circlePath2 = UIBezierPath(arcCenter: CGPoint(x: 33,y: 17), radius: CGFloat(5), startAngle: CGFloat(0), endAngle:CGFloat(M_PI * 2), clockwise: true)
        
        let shapeLayer2 = CAShapeLayer()
        shapeLayer2.path = circlePath2.CGPath
        
        //change the fill color
        shapeLayer2.fillColor = self.tintColor.CGColor
        //you can change the stroke color
        shapeLayer2.strokeColor = UIColor.clearColor().CGColor
        //you can change the line width
        shapeLayer2.lineWidth = 1.0
        
        self.layer.addSublayer(shapeLayer2)
    }
    
    func createEndCircle() {
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: 33,y: 69), radius: CGFloat(8), startAngle: CGFloat(0), endAngle:CGFloat(M_PI * 2), clockwise: true)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.CGPath
        
        //UIColor(red: 9, green: 165, blue: 201, alpha: 1)
        
        //change the fill color
        shapeLayer.fillColor = UIColor.clearColor().CGColor
        //you can change the stroke color
        shapeLayer.strokeColor = UIColor(red: 41.0/255, green: 128.0/255, blue: 185.0/255, alpha: 1.0).CGColor
        //you can change the line width
        shapeLayer.lineWidth = 1.0
        
        self.layer.addSublayer(shapeLayer)
        
        
        let circlePath2 = UIBezierPath(arcCenter: CGPoint(x: 33,y: 69), radius: CGFloat(5), startAngle: CGFloat(0), endAngle:CGFloat(M_PI * 2), clockwise: true)
        
        let shapeLayer2 = CAShapeLayer()
        shapeLayer2.path = circlePath2.CGPath
        
        //change the fill color
        shapeLayer2.fillColor = UIColor(red: 41.0/255, green: 128.0/255, blue: 185.0/255, alpha: 1.0).CGColor
        //you can change the stroke color
        shapeLayer2.strokeColor = UIColor.clearColor().CGColor
        //you can change the line width
        shapeLayer2.lineWidth = 1.0
        
        self.layer.addSublayer(shapeLayer2)
    }

}

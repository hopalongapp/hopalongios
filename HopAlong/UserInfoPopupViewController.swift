//
//  UserInfoPopupViewController.swift
//  HopAlong
//
//  Created by Ryan on 2/25/16.
//  Copyright © 2016 HopAlong. All rights reserved.
//

import UIKit
import MessageUI

class UserInfoPopupViewController: UIViewController, MFMessageComposeViewControllerDelegate {

    var name: String = ""
    var fieldOfStudy: String = ""
    var phoneNumber: String = ""
    var bio: String = ""
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var fieldOfStudyLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var bioLabel: UILabel!
    @IBOutlet weak var mutualFriendsLabel: UILabel!
    
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var messageButton: UIButton!
    
    @IBAction func message(sender: AnyObject) {
        let messageVC = MFMessageComposeViewController()
        
        messageVC.body = "Hey, I'm in the HopAlong ride with you";
        messageVC.recipients = [self.phoneNumber]
        messageVC.messageComposeDelegate = self;
        
        self.presentViewController(messageVC, animated: false, completion: nil)
    }
    
    @IBAction func call(sender: AnyObject) {
        if let url = NSURL(string: "tel://\(self.phoneNumber)") {
            UIApplication.sharedApplication().openURL(url)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.callButton.setImage(UIImage(named: "phone_filled")?.imageWithRenderingMode(.AlwaysTemplate), forState: .Normal)
        self.messageButton.setImage(UIImage(named: "speech_bubble_filled")?.imageWithRenderingMode(.AlwaysTemplate), forState: .Normal)
        
        self.nameLabel.text = self.name
        self.fieldOfStudyLabel.text = self.fieldOfStudy
        self.formatPhoneNumber()
        self.bioLabel.text = self.bio
        self.mutualFriendsLabel.text = "16 Mutual Friends"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func formatPhoneNumber() -> Bool {
        let newString = String(self.phoneNumber)
        let components = newString.componentsSeparatedByCharactersInSet(NSCharacterSet.decimalDigitCharacterSet().invertedSet)
        
        let decimalString = components.joinWithSeparator("") as NSString
        let length = decimalString.length
        let hasLeadingOne = length > 0 && decimalString.characterAtIndex(0) == (1 as unichar)

        var index = 0 as Int
        let formattedString = NSMutableString()
        
        if hasLeadingOne {
            formattedString.appendString("1 ")
            index += 1
        }
        if (length - index) > 3 {
            formattedString.appendFormat("(\(decimalString.substringWithRange(NSMakeRange(index, 3)))) ")
            index += 3
        }
        if length - index > 3 {
            formattedString.appendFormat("\(decimalString.substringWithRange(NSMakeRange(index, 3)))-")
            index += 3
        }
        
        formattedString.appendString(decimalString.substringFromIndex(index))
        self.phoneNumberLabel.text = String(formattedString)
        return false
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

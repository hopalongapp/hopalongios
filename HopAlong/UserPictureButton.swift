//
//  UserPictureButton.swift
//  HopAlong
//
//  Created by Ryan on 2/25/16.
//  Copyright © 2016 HopAlong. All rights reserved.
//

import UIKit

class UserPictureButton: UIButton {

    enum RiderStatus: Int {
        case ResponsibleForCar = 0
        case Confirmed = 1
        case Unconfirmed = 2
        case Driver = 3
        case None = 4
    }
    
    var userNumberOnScreen: Int

    let shapeLayer = CAShapeLayer()
    var subImage = UIImageView()
    
    init(frame: CGRect, userNumberOnScreen: Int, drawStroke: Bool, riderType: RiderStatus) {
        self.userNumberOnScreen = userNumberOnScreen
        
        super.init(frame: frame)
        
        let riderStatus = riderType
        
        self.clipsToBounds = false
        self.layer.cornerRadius = self.frame.height/2
        self.contentMode = UIViewContentMode.ScaleAspectFill
        
//        if drawStroke {
//            self.layer.borderWidth = 2.5
//            self.layer.borderColor = UIColor.whiteColor().CGColor
//        }
        
        let backgroundLayer = CALayer()
        backgroundLayer.frame = CGRectMake(0, 0, self.frame.width, self.frame.height)
        backgroundLayer.shadowOpacity = 0.4
        backgroundLayer.shadowOffset = CGSize(width: 0, height: 0)
        backgroundLayer.shadowRadius = 2
        backgroundLayer.cornerRadius = self.frame.height/2
        backgroundLayer.backgroundColor = UIColor.whiteColor().CGColor
        
        super.imageView!.backgroundColor = UIColor.whiteColor()
        super.imageView!.clipsToBounds = true
        super.imageView!.layer.cornerRadius = self.frame.height/2
        super.imageView!.contentMode = UIViewContentMode.ScaleAspectFill

        //UIGraphicsBeginImageContextWithOptions(CGSizeMake(), true, <#T##scale: CGFloat##CGFloat#>)
        subImage = UIImageView(frame: CGRectMake(self.bounds.maxX - 16, self.bounds.maxY - 16, 16, 16))
        subImage.clipsToBounds = true
        subImage.layer.cornerRadius = subImage.bounds.height/2
        subImage.layer.borderWidth = 1.0
        subImage.layer.borderColor = UIColor.whiteColor().CGColor
        subImage.backgroundColor = UIColor.whiteColor()
        subImage.exclusiveTouch = false
        subImage.contentMode = UIViewContentMode.ScaleAspectFill
        
        switch riderStatus {
        case .ResponsibleForCar:
            subImage.image = UIImage(named: "steering_wheel_filled")?.imageWithRenderingMode(.AlwaysTemplate)
            self.addSubview(subImage)
            self.bringSubviewToFront(subImage)
        case .Confirmed:
            subImage.image = UIImage(named: "checkmark_filled")?.imageWithRenderingMode(.AlwaysTemplate)
            self.addSubview(subImage)
            self.bringSubviewToFront(subImage)
        case .Unconfirmed:
            subImage.image = UIImage(named: "question_mark_filled")?.imageWithRenderingMode(.AlwaysTemplate)
            self.addSubview(subImage)
            self.bringSubviewToFront(subImage)
        case .Driver:
            subImage.image = UIImage(named: "steering_wheel_filled")?.imageWithRenderingMode(.AlwaysTemplate)
            self.addSubview(subImage)
            self.bringSubviewToFront(subImage)
        case .None:
            break
        }
        
        self.layer.insertSublayer(backgroundLayer, atIndex: 0)
    }

    required init?(coder aDecoder: NSCoder) {
        self.userNumberOnScreen = 0
        super.init(coder: aDecoder)
    }

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        
    }
    */

}

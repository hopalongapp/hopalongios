//
//  UserPictureMapOverlay.swift
//  HopAlong
//
//  Created by Ryan on 2/25/16.
//  Copyright © 2016 HopAlong. All rights reserved.
//

import UIKit

class UserPictureMapOverlay: UIImageView {

    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        
        self.layer.cornerRadius = self.frame.height/2
        self.clipsToBounds = true
        
    }
    

}

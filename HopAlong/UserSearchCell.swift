//
//  UserSearchCell.swift
//  HopAlong
//
//  Created by Ryan on 3/14/16.
//  Copyright © 2016 HopAlong. All rights reserved.
//

import UIKit

class UserSearchCell: UITableViewCell {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var info: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.userImage.layer.cornerRadius = self.userImage.bounds.height/2
        self.userImage.clipsToBounds = true
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
